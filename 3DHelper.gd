extends Node

func rotate_point(point : Vector3, pivot : Vector3, axis : Vector3, angle : float):
	return (point - pivot).rotated(axis,angle) + pivot

func raycast(origin : Vector3, direction : Vector3, distance : float, collision_mask : int):
	var direct_space_state = get_viewport().world.direct_space_state
	return direct_space_state.intersect_ray(origin,direction.normalized() * distance, [],collision_mask)

func signed_angle(a : Vector3, b : Vector3, up : Vector3 = Vector3.UP):
	return a.angle_to(b) * (-1 if a.cross(b).dot(Vector3.UP) < 0 else 1)

func snap_to_grid(point : Vector3, grid_size : float, plane : Vector3 = Vector3.ONE):
	point.x = stepify(point.x,grid_size * abs(sign(plane.x))) 
	point.y = stepify(point.y,grid_size * abs(sign(plane.y)))
	point.z = stepify(point.z,grid_size * abs(sign(plane.z))) 
	return point

func clamp_vector_angle_between_3d(vector : Vector3, reference_direction : Vector3, max_angle : float, up : Vector3 = Vector3.UP):
	var angle = signed_angle(vector,reference_direction)
	
	var l = vector.length()
	var result_angle = -clamp(angle,-max_angle,max_angle)
	var quat = Quat(up,result_angle)
	return quat * reference_direction
	
func clamp_vector_angle_between(vector : Vector2, limit_a : Vector2, limit_b : Vector2):
	var angle_a = limit_a.angle()
	var angle_b = limit_b.angle()
	
	var l = vector.length()
	var angle = clamp(vector.angle(),min(angle_a,angle_b),max(angle_a,angle_b))
	return Vector2.RIGHT.rotated(angle).normalized() * l
	
func absolute(a : Vector3):
	return Vector3(abs(a.x),abs(a.y),abs(a.z))
